const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: [true, "Username is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	address: {
		type: String,
		required: [true, "Address is required"]
	},
	contact: {
		type: String,
		required: [true, "Contact number is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	productOrders: [
		{
			productName: {type: String},
			productId: {type: String},
			quantity: {type: Number},
			address: {type: String},
			price: {type: Number},
			subtotal: {type: Number},
			orderedOn:{
			type: Date,
			default: new Date()
			},
			isPaid: {type: Boolean, default: false}
			
		}]
})


module.exports = mongoose.model("User", userSchema);