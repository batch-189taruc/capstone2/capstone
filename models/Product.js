const mongoose = require('mongoose')

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		
	},
	description: {
		type: String,
		
	},
	price: {
		type: Number,
		
	},
	stock: {
		type: Number,
		
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()

	},
	placedOrder: [
	{
		userId: {
			type: String,
			required: [true, "UserID is required"]},
		username:{type: String},
		name: {type: String},
		quantity: {type: Number},
		subtotal: {type: Number},
		orderTime:{
			type: Date,
			default: new Date()
		},
		isPaid: {type: Boolean, default: false }
	}]
})

module.exports = mongoose.model("Product", productSchema);