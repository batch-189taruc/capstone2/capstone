const Product = require('../models/Product.js');



//show all active products

module.exports.showAllActive = () =>{
	return Product.find({isActive: true}).then(result =>{
		return result
	})
}

//show 1 product

module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result
	})
}

//update a product
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stock: reqBody.stock
	}
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
}

//Archive a product

module.exports.archiveProduct = (data) => {

	return Product.findByIdAndUpdate(data.productId).then((result, err) => {

		if(data.payload === true) {
			result.isActive = false 

			return result.save().then((archivedProduct, err) => {
				if(err) {
					return false
				} else {
					return "archived"
				}
			})
		} else {
			return false
		}
	})
}
//unarchive a product

module.exports.unArchiveProduct = (data) => {

	return Product.findByIdAndUpdate(data.productId).then((result, err) => {

		if(data.payload === true) {
			result.isActive = true 

			return result.save().then((archivedProduct, err) => {
				if(err) {
					return false
				} else {
					return "unarchived"
				}
			})
		} else {
			return false
		}
	})
}

//delete a product

module.exports.deleteProduct = (reqParams) => {
	return Product.findByIdAndRemove(reqParams.productId)
}


//update a product
module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {
		productName: reqBody.productName,
		description: reqBody.description,
		price: reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, err) => {
		if(err) {
			return false
		} else {
			return true
		}
	})
}



//add product
module.exports.addProduct = (reqBody) => {
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stock: reqBody.stock
	
	});
	return newProduct.save().then((product, error) => {
		if(error){
			return false
		} else{
			return true
		}
	})
}

//show 1 product

module.exports.checkProduct = (reqBody) => {
	return  Product.findOne({name: reqBody.name}).then(result => {
		return result
	})
}

module.exports.viewOrders = (reqParams) => {
	return Product.findById(reqParams).then(result => {
		return result.placedOrder
	})
}
