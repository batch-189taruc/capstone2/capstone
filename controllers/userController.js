const User = require('../models/User.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js')
const Product = require('../models/Product.js');

//registration

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		username: reqBody.username,
		email: reqBody.email,
		address: reqBody.address,
		contact: reqBody.contact,
		isAdmin: reqBody.isAdmin,
		password: bcrypt.hashSync(reqBody.password, 10) //paramerters 
		//(<dataToBeHash>, <saltRound>)
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false
		} else{
			return true
		}
	})
}

//login
module.exports.loginUser = (reqBody) => {


	return User.findOne({email: reqBody.email}).then(result => {

		// User does not exist
		if (result == null) {
			return false

		
		} else {

			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			
			
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}

	})
}




//place an order


module.exports.addorder = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.productOrders.push({
			productName: data.productName,
			quantity: data.quantity,
			price: (data.subtotal / data.quantity),
			subtotal: data.subtotal
			});

		return user.save().then((user, error) => {

			if (error) {
				return false
			} else {
				return true
			}
		})

	})


	let isProductUpdated = await Product.findOne({name: data.productName}).then(product => {
		
		product.placedOrder.push({userId: data.userId, username: data.username, name: data.productName, quantity: data.quantity, subtotal: data.quantity * product.price});
		
		

		return product.save().then((product, error) => {

			if(error) {
				return false
			} else {
				return true
			}
		})

	})


	if (isUserUpdated && isProductUpdated) {

		return true
		
	} else {

		return false
	}

}

//show orders

module.exports.getOrders = (data) => {
	return User.findById(data.id).then(result => {
		if(result == null){
			return true
		}else{
			
			return result.productOrders
		}
	})
}






//delete my account
module.exports.deleteMyAccount = (reqBody) => {


	return User.findOne({email: reqBody.email}).then(result => {

		// User does not exist
		if (result == null) {
			return "User does not exist"

		
		} else {

			
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			
			
			if(isPasswordCorrect) {
				
				return User.findOneAndDelete({email: reqBody.email}) 
			} else {
				return "User does not exist"
			}
		}

	})
}

//delete an admin account

module.exports.deleteAdmin = (reqBody) => {


	return User.findOne({email: reqBody.email}).then(result => {

		// User does not exist
		if (result == null) {
			return "Admin account does not exist"

		
		} else {
	
				return User.findOneAndDelete({email: reqBody.email}) 
			
		}

	})
}



//update own user info

module.exports.updateMyInfo = (userData, reqBody) => {
	let updatedUser = {
		email: reqBody.email,
		address: reqBody.address,
		contact: reqBody.contact
		
	}
	return User.findByIdAndUpdate(userData.id, updatedUser).then((user,error) =>{
		if(error){
			return "Unauthorized action - you're not an admin."
		}else{
			return "Update has been completed!"
			
		}
	})
}

//Retrieve details
module.exports.getProfile = (data) => {

	return User.findById(data.id).then(result => {

		//console.log(result)

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};

module.exports.checkEmailExists = (reqBody) => {

	
	return User.find({email: reqBody.email}).then(result => {

		
		if (result.length > 0) {
			return true

		
		} else {
			return false
		}

	})

}

//retrieve all users
module.exports.getAllUsers = () =>{
	return User.find({}).then(result =>{
		return result
	})
}

//retrieve all spices
module.exports.getAllSpices = () =>{
	return Product.find({}).then(result =>{
		return result
	})
}

//retrieve 1 user as admin
module.exports.adminGetUser = (reqParams) => {
	return User.findById(reqParams.userId).then(result => {
		console.log(result)
		return result
	})
}

//retrieve 1 product as admin
module.exports.adminGetProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		console.log(result)
		return result
	})
}

//update user as an admin
module.exports.updateUser1 = (reqParams) => {
	let updatedUser = {
		isAdmin: true
	}
	
	return User.findByIdAndUpdate(reqParams.userId,updatedUser)
}
//remove user as an admin
module.exports.updateUser2 = (reqParams) => {
	let updatedUser = {
		isAdmin: false
	}
	
	return User.findByIdAndUpdate(reqParams.userId,updatedUser)
}

//delete a user
module.exports.deleteUser = (reqParams) => {
		return User.findByIdAndRemove(reqParams)
}

//ORIGINAL GETALLORDERS
module.exports.getAllOrders = () =>{
	return Product.find({}).select("placedOrder").then(result =>{
		return result
	})
}

